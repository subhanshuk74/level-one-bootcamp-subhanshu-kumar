//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>
float tomboloide(float h, float b, float d);

int main(){
  float h, b, d, volume;
  printf("Enter the height: ");
  scanf("%f",&h);
  printf("Enter the breadth: ");
  scanf("%f",&b);
  printf("Enter the depth: ");
  scanf("%f",&d);
  volume = tomboloide(h,b,d);
  printf("The volume of tomboloide is: %0.2f its height: %0.2f, bredth: %0.2f and depth: %0.2f \n",volume,h,b,d);
  return 0;
}

float tomboloide(float h, float b, float d){
  return ((h*d*b)+(d/b))/3;
}
