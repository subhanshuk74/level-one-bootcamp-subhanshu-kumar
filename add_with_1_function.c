//Write a program to add two user input numbers using one function.
#include <stdio.h>
int sum(float x, float y);

int main(){
  float a,b,c;
  printf("Enter two no. for sum:");
  scanf("%f %f", &a, &b);
  c = sum(a,b);
  printf("The sum of %f + %f = %f",a,b,c);
  return 0;
}

int sum(float x, float y){
    float z;
    z = x + y;
    return z;
}
