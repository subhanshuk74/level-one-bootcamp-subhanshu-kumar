//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>

struct coordinate{
  float x;
  float y;
};
typedef struct coordinate point;

point input();
float distance(point p1, point p2);
float output(point p1, point p2, float dist);

int main() {
  point p1, p2;
  float d;
  printf("---------------------------------\n");
  printf("Enter the coordinate of 1st point\n");
  printf("---------------------------------\n");
  p1 = input();
  printf("---------------------------------\n");
  printf("Enter the coordinate of 2nd point\n");
  printf("---------------------------------\n");
  p2 = input();
  printf("---------------------------------\n");
  d = distance(p1,p2);
  output(p1,p2,d);

  return 0;
}

point input(){
  point p;
  printf("Enter x coordinate :");
  scanf("%f",&p.x);
  printf("Enter y coordinate :");
  scanf("%f",&p.y);
  return p;
}

float distance(point p1, point p2){
  return (sqrt(((p2.x-p1.x)*(p2.x-p1.x))+((p2.y-p1.y)*(p2.y-p1.y))));
}

float output(point p1, point p2, float dist){
  printf("The coordinate of 1st point : (%0.2f, %0.2f)\n",p1.x,p1.y);
  printf("The coordinate of 2nd point : (%0.2f, %0.2f)\n",p2.x,p2.y);
  printf("The distance between two points: %0.2f\n",dist);
  return 0;
}
