//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
float input();
float distance(float a1, float a2, float b1, float b2);
float output(float x1, float x2, float y1, float y2, float dist);

int main() {
  float x1, y1, x2, y2;
  float d;
  printf("---------------------------------\n");
  printf("Enter the coordinate of 1st point\n");
  printf("---------------------------------\n");
  printf("x1: ");
  x1 = input();
  printf("y1: ");
  y1 = input();
  printf("---------------------------------\n");
  printf("Enter the coordinate of 2nd point\n");
  printf("---------------------------------\n");
  printf("x2: ");
  x2 = input();
  printf("y2: ");
  y2 = input();
  printf("---------------------------------\n");
  d = distance(x1,x2,y1,y2);
  output(x1,x2,y1,y2,d);

  return 0;
}

float input(){
  float point;
  scanf("%f",&point);
  return point;
}

float distance(float a1, float a2, float b1, float b2){
  return (sqrt(((a2-a1)*(a2-a1))+((b2-b1)*(b2-b1))));
}

float output(float x1, float x2, float y1, float y2, float dist){
  printf("The coordinate of 1st point : (%0.2f, %0.2f)\n",x1,y1);
  printf("The coordinate of 2nd point : (%0.2f, %0.2f)\n",x2,y2);
  printf("The distance between two points: %0.2f\n",dist);
  return 0;
}
