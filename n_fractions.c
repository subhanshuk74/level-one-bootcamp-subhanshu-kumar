//WAP to find the sum of n fractions.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct fraction{
  int n;
  int d;
};
typedef struct fraction frac;

frac input();
frac sum(frac fract1, frac fract2);
int gcd(int p, int q);
int output(frac s);

int main(){
  int n,i;
  frac a,z;
  printf("Enter the no. of terms: ");
  scanf("%d",&n);
  frac f[n];
  for(i=0;i<n;i++){
    printf("Enter farction %d\n",i+1);
    f[i] = input();
  }
  if(n==1){
    z = f[0];
  }
  else{
    a = f[0];
    for(i=0;i<n-1;i++){
      z = sum(a,f[i+1]);
      a = z;
    }
  }
  output(z);
  return 0;
}

frac input(){
  frac f;
  printf("N : ");
  scanf("%d",&f.n);
  printf("--  --\n");
  printf("D : ");
  scanf("%d",&f.d);
  return f;
}

int gcd(int p, int q){
  int i,a;
  for(i = 1; i <= p && i <= q; i++)
    {
        if(p % i == 0 && q % i == 0)
            a = i;
    }
    return a;
}

frac sum(frac fract1, frac fract2){
  frac fract3;
  int c;
  fract3.n = ((fract1.n*fract2.d)+(fract2.n*fract1.d));
  fract3.d = fract1.d*fract2.d;
  c = gcd(fract3.n,fract3.d);
  fract3.n = fract3.n/c;
  fract3.d = fract3.d/c;
  return fract3;
}

int output(frac s){
  printf("The sum of the given fractions : %d / %d\n",s.n,s.d);
  return 0;
}
