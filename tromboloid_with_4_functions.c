//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input();
float tomboloide(float h, float b, float d);
float output(float h,float b, float d, float z);

int main(){
  float h, b, d, volume;
  printf("Enter the height: ");
  h = input();
  printf("Enter the breadth: ");
  b = input();
  while(b <= 0){
    printf("Again enter the breadth :");
    b = input();
  }
  printf("Enter the depth: ");
  d = input();
  while(d <= 0){
    printf("Again enter the depth :");
    d = input();
  }
  volume = tomboloide(h,b,d);
  output(h,b,d,volume);
  return 0;
}

float input(){
  float i;
  scanf("%f",&i );
  return i;
}

float tomboloide(float h, float b, float d){
  return ((h*d*b)+(d/b))/3;
}

float output(float h,float b, float d, float z){
  printf("Volume of tumboloid: %0.2f its height: %0.2f, bredth: %0.2f and depth: %0.2f \n",z,h,b,d );
  return 0;
}
