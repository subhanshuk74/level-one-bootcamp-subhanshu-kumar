//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
float input();
float sum(float q, float r);
float print(float x,float y,float z);

int main(){
  float a,b,c;

  printf("Enter first no. : ");
  a = input();
  printf("Enter second no. : ");
  b = input();
  c = sum(a,b);
  print(a,b,c);
  return 0;
}

float input(){
  float v;
  scanf("%f",&v);
  return v;
}

float sum(float q, float r){
  return (q + r);
}

float print(float x,float y, float z){
  printf("%f + %f = %f\n",x,y,z );
  return 0;
}
